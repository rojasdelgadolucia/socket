package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
public class ClienteTCP {
    public static void main(String[] args) {
        try {
            Socket cliente = new Socket("localhost", 55000);
            System.out.println("Cliente conectado al servidor.");
            System.out.println("Introduce uno de los operandos:+, - , * , /" + "Ejemplo= 5 + 5");

            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

            // Hilo para recibir mensajes del servidor
            Thread recibirMensajesThread = new Thread(() -> {
                try {
                    while (true) {
                        String mensaje = entrada.readUTF();
                        System.out.println("Servidor: " + mensaje);
                        System.out.println("Introduce uno de los operandos:+, - , * , /" + "Ejemplo= 5 + 5");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            recibirMensajesThread.start();

            // Lógica para enviar mensajes al servidor
            Scanner scanner = new Scanner(System.in);
            while (true) {
                String userInput = scanner.nextLine();
                salida.writeUTF(userInput);

                // Si el usuario ingresa "salir", termina la conexión
                if (userInput.equalsIgnoreCase("salir")) {
                    break;
                }
            }

            // Espera a que el hilo de recepción de mensajes termine antes de cerrar la conexión
            recibirMensajesThread.join();

            // Cierra las conexiones
            entrada.close();
            salida.close();
            cliente.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
