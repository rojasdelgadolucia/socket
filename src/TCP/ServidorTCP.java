package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class ServidorTCP {
    private static final int PUERTO = 55000;
    private static final int MAX_CLIENTES = 5;
    private static final Semaphore semaforo = new Semaphore(MAX_CLIENTES);
    private static final ExecutorService pool = Executors.newFixedThreadPool(MAX_CLIENTES);

    public static void main(String[] args) {
        try {
            ServerSocket servidor = new ServerSocket(PUERTO);
            System.out.println("Servidor iniciado. Esperando conexiones...");

            while (true) {
                semaforo.acquire(); // Espera a que haya un permiso disponible
                Socket cliente = servidor.accept();
                System.out.println("Conexión establecida con cliente: " + cliente.getInetAddress());

                pool.execute(new ClienteHandler(cliente, semaforo));
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class ClienteHandler implements Runnable {
        private final Socket clienteSocket;
        private final Semaphore semaforo;

        ClienteHandler(Socket clienteSocket, Semaphore semaforo) {
            this.clienteSocket = clienteSocket;
            this.semaforo = semaforo;
        }

        @Override
        public void run() {
            try {
                DataInputStream entrada = new DataInputStream(clienteSocket.getInputStream());
                DataOutputStream salida = new DataOutputStream(clienteSocket.getOutputStream());

                System.out.println("Cliente conectado: " + clienteSocket.getInetAddress());

                while (true) {
                    String mensajeCliente = entrada.readUTF();
                    System.out.println("Cliente dice: " + mensajeCliente);

                    // Si el cliente quiere salir, termina la conexión
                    if (mensajeCliente.equalsIgnoreCase("salir")) {
                        System.out.println("Cliente salió: " + clienteSocket.getInetAddress());
                        salida.writeUTF("Adiós");
                        break;
                    }

                    // Realizar operaciones matemáticas
                    String[] partes = mensajeCliente.split(" ");
                    if (partes.length != 3) {
                        salida.writeUTF("Formato de entrada incorrecto. Debe ser 'número operador número'");
                        continue;
                    }

                    double num1 = Double.parseDouble(partes[0]);
                    double num2 = Double.parseDouble(partes[2]);
                    char operador = partes[1].charAt(0);

                    double resultado = 0;
                    switch (operador) {
                        case '+':
                            resultado = num1 + num2;
                            break;
                        case '-':
                            resultado = num1 - num2;
                            break;
                        case '*':
                            resultado = num1 * num2;
                            break;
                        case '/':
                            if (num2 != 0) {
                                resultado = num1 / num2;
                            } else {
                                salida.writeUTF("Error: División por cero");
                                continue;
                            }
                            break;
                        default:
                            salida.writeUTF("Operador no válido. Use '+', '-', '*' o '/'");
                            continue;
                    }

                    salida.writeUTF("El resultado es: " + resultado);
                }

                // Cierra las conexiones
                entrada.close();
                salida.close();
                clienteSocket.close();
                semaforo.release(); // Liberar el permiso

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
