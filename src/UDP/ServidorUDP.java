package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServidorUDP {
    private static final int PUERTO = 55000;

    public static void main(String[] args) {
        try {
            DatagramSocket socket = new DatagramSocket(PUERTO);
            System.out.println("Servidor UDP iniciado. Esperando conexiones...");

            while (true) {
                byte[] buffer = new byte[1024];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

                socket.receive(packet);

                String mensaje = new String(packet.getData(), 0, packet.getLength());
                System.out.println("Mensaje recibido: " + mensaje);

                // Verifica si el mensaje es "salir"
                if (mensaje.equalsIgnoreCase("salir")) {
                    System.out.println("Cliente solicitó salir. Cerrando el servidor...");
                    break;
                }

                // Procesa el mensaje recibido y envía una respuesta
                String respuesta = procesarMensaje(mensaje);

                byte[] respuestaBytes = respuesta.getBytes();
                DatagramPacket respuestaPacket = new DatagramPacket(respuestaBytes, respuestaBytes.length, packet.getAddress(), packet.getPort());
                socket.send(respuestaPacket);
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String procesarMensaje(String mensaje) {
        String[] partes = mensaje.split(" ");

        if (partes.length != 3) {
            return "Formato incorrecto. Debe ser:número operador número";
        }

        try {
            double num1 = Double.parseDouble(partes[0]);
            double num2 = Double.parseDouble(partes[2]);
            char operador = partes[1].charAt(0);

            double resultado = 0;
            switch (operador) {
                case '+':
                    resultado = num1 + num2;
                    break;
                case '-':
                    resultado = num1 - num2;
                    break;
                case '*':
                    resultado = num1 * num2;
                    break;
                case '/':
                    if (num2 == 0) {
                        return "Error: división por cero";
                    }
                    resultado = num1 / num2;
                    break;
                default:
                    return "Operador no válido. Use '+', '-', '*' o '/'";
            }

            return String.valueOf(resultado);
        } catch (NumberFormatException e) {
            return "Error: formato de número no válido";
        }
    }

}