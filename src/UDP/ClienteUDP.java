package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

public class ClienteUDP {
    private static final int PUERTO = 55000;

    public static void main(String[] args) {
        try {
            DatagramSocket socket = new DatagramSocket();
            InetAddress serverAddress = InetAddress.getByName("localhost");

            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.print("Cliente <numero> <operador> <numero>:");
                String input = scanner.nextLine();

                // Envía el mensaje al servidor
                byte[] mensajeBytes = input.getBytes();
                DatagramPacket packet = new DatagramPacket(mensajeBytes, mensajeBytes.length, serverAddress, PUERTO);
                socket.send(packet);

                if (input.equalsIgnoreCase("salir")) {
                    System.out.println("Cliente desconectado.");
                    break;
                }

                // Espera la respuesta del servidor
                byte[] buffer = new byte[1024];
                DatagramPacket respuestaPacket = new DatagramPacket(buffer, buffer.length);
                socket.receive(respuestaPacket);

                // Obtiene y muestra la respuesta del servidor
                String respuesta = new String(respuestaPacket.getData(), 0, respuestaPacket.getLength());
                System.out.println("Servidor: " + respuesta);
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
